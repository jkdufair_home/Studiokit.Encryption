<Query Kind="Statements">
  <Output>DataGrids</Output>
  <Reference Relative="bin\Debug\Microsoft.WindowsAzure.Configuration.dll">C:\git\forecast\StudioKit.Encryption\bin\Debug\Microsoft.WindowsAzure.Configuration.dll</Reference>
  <Reference Relative="bin\Debug\StudioKit.Encryption.dll">C:\git\forecast\StudioKit.Encryption\bin\Debug\StudioKit.Encryption.dll</Reference>
  <Reference>&lt;RuntimeDirectory&gt;\System.Security.dll</Reference>
  <Namespace>StudioKit.Encryption</Namespace>
  <Namespace>System.Security.Cryptography.Pkcs</Namespace>
  <Namespace>System.Security.Cryptography.X509Certificates</Namespace>
</Query>

// Note: copy the thumbprint for your application's encrypted_config certificate
// DO NOT SAVE ANY SENSITIVE DATA TO THIS FILE
var thumbprint = "B38E0C77FC097F38ACEB5F859EBD618691A7DDC4";

var settingsToEncrypt = new List<string> {
 	"Test"
};
	
settingsToEncrypt.Select(s => new {
	Value = s, 
	EncryptedValue = EncryptedConfigurationManager.Encrypt(thumbprint, s)
})
.Dump();

var settingsToDecrypt = new List<string> {
"MIIBtAYJKoZIhvcNAQcDoIIBpTCCAaECAQAxggFtMIIBaQIBADBRMD0xOzA5BgNVBAMeMgBmAG8AcgBlAGMAYQBzAHQAXwBlAG4AYwByAHkAcAB0AGUAZABfAGMAbwBuAGYAaQBnAhAeyQhN0c+qvUPQpXMQcfDuMA0GCSqGSIb3DQEBAQUABIIBAH1O2EIMDLnpvqr5ZITY9Y8QzY55q64RvMmngRFX8iH3Jn6SM0IRk4Mcci0xc0cLNRzZZ048qGrX6FP0HvB2Lc9Lby1XBlxow0HAeuHmFEG4VtwDcJU38v3DFIpiFICh4vgF9XCUkwg5zc0Tu1CvQ96Pql5AvvTpv7TYuHRO5awnKKqWkruqFxTq9P5DMs/UFqXJzTruAhIlb2l9RP6+ZgOaOhTnpw7WTU1avTc3jNOUZjT0U8CVcXYt1IEibx9lOeCbjV3FXvXKyXijYQx6NYgMArHN8yR9kyxmw+J2IMMlxyYfjzIn6jafid9+tI99/RI/8uO7zKln0msm7/esRPYwKwYJKoZIhvcNAQcBMBQGCCqGSIb3DQMHBAgehIlmbVwV/IAIw78615j2yPQ="
};

	
settingsToDecrypt.Select(s => EncryptedConfigurationManager.Decrypt(s))
.Dump();